agg = Hash.new { |h, k|  h[k] = [] }
statuses = []

Variant.find_each do |variant|
begin
  name = "#{variant.name} - #{variant.gene.name}"
  resp = MyVariantInfo.new(variant.id).response
  if resp.empty?
    agg[:no_match] << name
  else
    resp = JSON.parse(resp)
    if resp['clinvar'].present? && resp['clinvar']['rcv'].present?
      status = if resp['clinvar']['rcv'].is_a?(Hash)
                [resp['clinvar']['rcv']]
               else
                resp['clinvar']['rcv']
               end
      if status.any? { |x| statuses << x['review_status'] ;x['review_status'].present? && x['review_status'] == 'reviewed by expert panel' }
        agg[:good_score] << name
      else
        agg[:bad_score] << name
      end
    else
      agg[:match_with_no_clinvar] << name
    end
  end
rescue StandardError => e
  binding.pry
end
end

File.open('clinvar_stats', 'w') do |f|
  f.puts "No MyVariant.info match: #{agg[:no_match].size}"
  f.puts "No ClinVar info: #{agg[:match_with_no_clinvar].size}"
  f.puts "ClinVar rating < 3: #{agg[:bad_score].size}"
  f.puts "ClinVar rating >= 3: #{agg[:good_score].size}"
  f.puts agg[:good_score].join(', ')
end
