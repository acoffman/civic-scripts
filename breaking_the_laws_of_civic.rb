def process_evidence
  eids = EvidenceItem.includes(:submitter, :acceptor).select do |eid|
    eid.submitter == eid.acceptor
  end

  File.open('eids_accepted_by_submitter.tsv', 'w') do |f|
    f.puts ['civic_lawbreaker', 'eid', 'url'].join("\t")
    eids.each do |eid|
      f.puts [eid.acceptor.username, eid.id, url_for_eid(eid)].join("\t")
    end
  end
end

def url_for_eid(eid)
  "https://civic.genome.wustl.edu/#/events/genes/#{eid.variant.gene_id}/summary/variants/#{eid.variant_id}/summary/evidence/#{eid.id}/summary"
end

def process_changesets
  changes = Event.where(action: 'change accepted').map do |e|
    if sc = SuggestedChange.find_by(id: e.state_params['suggested_change']['id'])
      acceptor = e.originating_user
      submitter = sc.originating_user
      if submitter == acceptor && sc.moderated && sc.moderated.last_applied_change == sc
        sc
      else
        nil
      end
    else
      puts e.id
    end
  end.compact

  File.open('changes_accepted_by_submitter.tsv', 'w') do |f|
    f.puts ['civic_lawbreaker', 'change size', 'url',].join("\t")
    changes.sort_by { |x| - calculate_changeset_size(x) }.each do |change|
      f.puts [change.originating_user.username, calculate_changeset_size(change), url_for_changeset(change)].join("\t")
    end
  end
end

def url_for_changeset(sc)
  id_type = sc.moderated.class.to_s.downcase
  id = sc.moderated.id
  detail_url = FrontendRouter.new(id_type, id).url
  detail_url.gsub(/summary$/, "talk/revisions/list/#{sc.id}/summary")
  #https://civic.genome.wustl.edu/#/events/genes/2478/summary/variants/767/talk/revisions/list/3140/summary
end

def calculate_changeset_size(sc)
  sc.suggested_changes.values.inject(0) do |sum, (old, new)|
    sum += levenshtein_distance(old || '', new || '')
  end
end

def levenshtein_distance(s, t)
  m = s.to_s.length
  n = t.to_s.length
  return m if n == 0
  return n if m == 0
  d = Array.new(m+1) {Array.new(n+1)}

  (0..m).each {|i| d[i][0] = i}
  (0..n).each {|j| d[0][j] = j}
  (1..n).each do |j|
    (1..m).each do |i|
      d[i][j] = if s[i-1] == t[j-1]  # adjust index into string
                  d[i-1][j-1]       # no operation required
                else
                  [ d[i-1][j]+1,    # deletion
                    d[i][j-1]+1,    # insertion
                    d[i-1][j-1]+1,  # substitution
                  ].min
                end
    end
  end
  d[m][n]
end

#process_evidence
process_changesets
