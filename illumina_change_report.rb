illumina_user = User.find(48)
scs = SuggestedChange.where(user: illumina_user).includes(:comments, :moderated)

headers = [
  'gene',
  'variant',
  'rid',
  'status',
  'link',
  'fields',
  'proposed_values',
  'comments'
]

File.open('illumina_suggested_changes.tsv', 'w') do |f|
  f.puts headers.join("\t")

  scs.each do |sc|
    f.puts [
      sc.moderated.gene.name,
      sc.moderated.name,
      sc.id,
      sc.status,
      FrontendRouter.new('variant', sc.moderated_id).url,
      sc.suggested_changes.keys.map { |k| k.gsub('_id', '') }.join("/"),
      DiffPresenter.new(sc.suggested_changes).as_json.values.map { |x| x[:final] }.join('/'),
      sc.comments.map(&:text).join("/").gsub("\n", ' ')
    ].join("\t")
  end
end