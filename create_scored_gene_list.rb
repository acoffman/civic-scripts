def scored_gene_list_headers
  [
    'Gene',
    'Scored Variant Name',
    'CIViC Score'
  ]
end

def create_scored_gene_list
  File.open('genes_by_civic_score.tsv', 'w') do |f|
    f.puts scored_gene_list_headers.join("\t")

    genes = Gene.joins(:variants).where("evidence_items.status = 'accepted'")
      .eager_load(variants: [:evidence_items])

    genes_with_scores = genes.map do |gene|
      (variant, score) = get_highest_variant_score_for_gene(gene)
      [gene.name, variant.name, score]
    end

    genes_with_scores.sort_by { |(_, _, score)| -score }.each do |(gene, variant, score)|
      f.puts [
        gene,
        variant,
        score
      ].join("\t")
    end
  end
end

def get_highest_variant_score_for_gene(gene)
  gene.variants.map { |v| [v, Actions::CalculateCivicScore.new(v).perform] }
    .sort_by { |(_, score)| -score }.first
end

create_scored_gene_list
