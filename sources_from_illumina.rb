File.open('illumina_source_report.tsv', 'w') do |f|
  f.puts "url\tcitation\teid count\tidentical statement count"
  Source.joins(evidence_items: [:submitter])
    .where('events.originating_user_id = ?', User.last.id)
    .where('evidence_items.status = ?', 'submitted')
    .sort_by { |s| s.evidence_items.where(status: 'submitted').count }
    .map do |source|
      [
        "http://138.197.108.59/#/sources/#{source.id}/summary",
        source.name,
        source.evidence_items.count,
         source.evidence_items.map(&:description).each_with_object(Hash.new(0)) { |desc, h| h[desc] += 1 }.values.max
      ]
    end.sort_by { |x| - x[2] }.uniq.each do |line|
      f.puts line.join("\t")
    end
  end
