fields = [
  'chromosome',
  'start',
  'stop',
  'reference_bases',
  'variant_bases',
  'representative_transcript',
  'chromosome2',
  'start2',
  'stop2',
]

File.open('civic_genomic_hgvs.tsv', 'w') do |f|
 headers = fields + ['genomic_hgvs']
 f.puts headers.join("\t")
 Variant.all.each do |var|
   row = []
   fields.each { |field| row << var.send(field) }
   row << HgvsExpression.my_gene_info_hgvs(var)
   f.puts row.join("\t")
 end
end

