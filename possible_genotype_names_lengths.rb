histogram = Hash.new { |h, k| h[k] = [0, nil] }

Variant.eager_load(:gene).all.map { |v| "#{v.gene.name} #{v.name}" }.combination(3).each do |name|
  y = name.join(" and ")
  x = histogram[y.size]
  x[0] +=1
  x[1] = y
end

File.open('name_lengths.tsv', 'w') do |f|
  f.puts ['name_length', 'count', 'example'].join("\t")
  histogram.sort_by { |length, _| length }.each do |(length, (count, example))|
    f.puts [length, count, example].join("\t")
  end
end