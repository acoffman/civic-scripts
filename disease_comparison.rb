def clean_name(name)
  name.gsub("'s", '')
    .gsub("'", '')
    .gsub('-', ' ')
    .gsub('"','')
    .gsub('(disorder)','')
    .gsub('/',' ')
    .downcase
    .strip
    .split(' ')
    .reject {|x| x == 'of' || x == 'the' }
    .sort
end

civic_diseases = {}
Disease.all.each do |d|
  civic_diseases[clean_name(d.display_name)] =  [d.doid, d.name]
  d.disease_aliases.all.each do |da|
    civic_diseases[clean_name(da.name)] =  [d.doid, d.name]
  end
end

match_count = 0
no_match_count = 0
File.open('illumina_to_doid.tsv', 'w') do |f|
  f.puts ['illumina_name', 'civic_doid_name', 'doid'].join("\t")
  File.open('llumina_disease_list.tsv', 'r').each_line do |line|
    line.downcase!
    if (match = civic_diseases[clean_name(line)]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('cancer', 'carcinoma'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('cancer', 'disease'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('disease', 'cancer'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('tumor', 'cancer'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('cancer', 'tumor'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    elsif (match = civic_diseases[clean_name(line.gsub('carcinoma', 'cancer'))]).present?
      f.puts [line.strip, match[1], match[0]].join("\t")
      match_count +=1
    else
      f.puts [line.strip, '', ''].join("\t")
      no_match_count +=1
    end
  end
end
puts "Match Count: #{match_count}"
puts "Unmatched Count: #{no_match_count}"