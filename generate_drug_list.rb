File.open("all_civic_drugs.tsv", "w") do |f|
  f.puts ['ID', 'Name', 'Wikidata'].join("\t")
  Drug.all.each do |d|
    f.puts [d.id, d.name, d.wikidata_entity_id.present? ? "http://www.wikidata.org/entity/#{d.wikidata_entity_id}" : ''].join("\t")
  end
end