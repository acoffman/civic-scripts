require 'csv'

#at a cutoff of 20, 10, and 5 panels, and at civic scores of 20, 30, 40
  #genes in both
  #genes in civic panel but not in candidate list
  #genes in candidate list, but not panel

def civic_score_cutoffs
  [20, 30, 40, 50]
end

def panel_count_cutoff
  [1, 5, 10, 20]
end

def headers
  [
    'CIViC Variant Score Cutoff',
    'Quantity of Gene Panels Cutoff',
    'Number of Genes in CIViC Panel',
    'Number of Genes in Candidates File',
    'Number of Genes Only In CIViC Panel',
    'Number of Genes Only In Candidates File',
    'Number of Genes In Both',
    'List of Genes Only In CIViC Panel',
    'List of Genes Only In Candidates File',
    'List of Genes In Both'
  ]
end

def get_highest_variant_score_for_gene(gene)
  gene.variants.map { |v| [v, Actions::CalculateCivicScore.new(v).perform] }
    .sort_by { |(_, score)| -score }.first
end

def process_candidates_file
end

def scored_civic_genes
  genes = Gene.joins(variants: [:evidence_items])
    .where("evidence_items.status = 'accepted'")
    .eager_load(variants: [:evidence_items])

  genes.each_with_object({}) do |gene, h|
    (_, score) = get_highest_variant_score_for_gene(gene)
    h[gene.name] = score
  end
end

def candidate_genes_with_panel_count(candidates_file)
  {}.tap do |h|
    CSV.foreach(candidates_file, col_sep: "\t", headers: true) do |row|
      h[row['gene_name']] = Integer(row['panel_count'])
    end
  end
end

def bucket_genes(civic_genes, candidate_genes, civic_cutoff, panel_count_cutoff)
  genes_in_civic_panel = civic_genes.select do |name, score|
    score >= civic_cutoff
  end

  genes_in_candidates_file = candidate_genes.select do |name, panel_count|
    panel_count >= panel_count_cutoff
  end

  genes_only_in_civic = genes_in_civic_panel.keys - genes_in_candidates_file.keys
  genes_only_in_candidates_file = genes_in_candidates_file.keys - genes_in_civic_panel.keys
  genes_in_both = genes_in_candidates_file.keys & genes_in_civic_panel.keys

  [
    civic_cutoff,
    panel_count_cutoff,
    genes_in_civic_panel.count,
    genes_in_candidates_file.count,
    genes_only_in_civic.count,
    genes_only_in_candidates_file.count,
    genes_in_both.count,
    genes_only_in_civic.join("|"),
    genes_only_in_candidates_file.join("|"),
    genes_in_both.join("|"),
  ]
end

def main(candidate_gene_file)
  civic_genes = scored_civic_genes
  candidate_genes = candidate_genes_with_panel_count(candidate_gene_file)
  File.open('civic_panel_vs_candidate_genes.tsv', 'w') do |f|
    f.puts headers.join("\t")
    civic_score_cutoffs.each do |civic_score_cutoff|
      panel_count_cutoff.each do |panel_count_cutoff|
        bucketed_genes = bucket_genes(civic_genes, candidate_genes, civic_score_cutoff, panel_count_cutoff)
        f.puts bucketed_genes.join("\t")
      end
    end
  end
end

candidate_gene_file = 'RankedCivicGeneCandidates.tsv'
main(candidate_gene_file)