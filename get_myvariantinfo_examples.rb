fields = MyVariantInfo.new(1).primary_fields + MyVariantInfo.new(1).secondary_fields

examples = {}
Variant.find_each do |variant|
  begin
  info = MyVariantInfo.new(variant.id)
  next if info.response.empty?
  response = JSON.parse(info.response)

  fields.each do |field|
begin
    val = response
    field.split('.').each do |key|
      if val.is_a?(Array)
        val = val.first
      end
      unless(val = val[key]).present?
        break
      end
    end
    if val.present?
      examples[field] = [HgvsExpression.my_gene_info_hgvs(variant), variant, val]
      fields.delete(field)
    end
rescue => e
  binding.pry
end
  end
  if fields.empty?
    break
  end
  rescue StandardError
  end
end


File.open('example_respones.tsv', 'w') do |f|
  f.puts "field\thgvs\tvariant_id\tentrez_id\tvalue"
  examples.each do |field, (hgvs, variant, val)|
    f.puts [field, hgvs, variant.id, variant.gene.entrez_id, val].join("\t")
  end
end

puts "Leftovers: #{fields.join(', ')}"