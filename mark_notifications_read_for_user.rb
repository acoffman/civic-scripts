Notification.where(
  notified_user_id: 6,
  originating_user_id: 48,
  type: Notification.types['subscribed_event'],
  seen: false
).each { |x| x.seen = true; x.save }
