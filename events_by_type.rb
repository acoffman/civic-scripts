timespan = 24.hours.ago
events = Event.where('created_at >= ?', timespan)

File.open('output', 'w') do |f|
  events.group_by { |e| e.originating_user }.sort_by { |(_, activity)| -activity.size }.each do |(user, activity)|
    grouped_activity = activity.group_by { |e| e.action }
    f.puts "#{user.display_name}\tTotal Actions: #{activity.size}"
    grouped_activity.sort_by { |(_, ga)| -ga.size }.each do |(action, action_events)|
      f.puts "#{action}: #{action_events.count}"
    end
  end
end