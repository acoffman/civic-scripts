VariantGroup.all.each do |vg|
  variants = vg.variants.uniq
  vg.variants = []
  vg.save
  vg.variants = variants
  vg.save
end