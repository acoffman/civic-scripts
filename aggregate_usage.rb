require 'set'

internal_users = Set.new([
"MalachiGriffith",
"obigriffith",
"bainscou",
"CodyRamirez",
"kkrysiak",
"NickSpies",
"LynzeyK",
"jmcmichael",
"kcampbel",
"gatoravi",
"MatthewMatlock",
"ahwagner",
"arpaddanos",
"acoffman",
"zlskidmore",
"emardis",
"rbilski",
"ConnorLiu",
"LeeT",
"ebarnell",
"YangYangFeng",
"kaitlinaclark",
"AmberWollam",
"DavidSpencer",
"iamarillo",
"KelsyCotto",
"fgomez",
"rlesurf",
"rbose",
"jhundal_1" ]) 

base_query = Event.eager_load(:originating_user)
  .order(created_at: :asc)

event_types = ['submitted', 'commented', 'change suggested', 'submitted']
date_types = [['week', '%U'], ['month', '%-m']]

event_types.each_with_index do |event_type, i|
  date_types.each do |(date_type, date_format)|
    events = base_query.where(action: event_type)
if event_type == 'submitted' &&  i == 3
  events = events.select { |x| x.subject.present? && x.subject.status == 'accepted' }
  event_type = 'accepted'
end
    by_date_and_user = events.chunk { |e| [e.created_at.strftime(date_format), e.created_at.year] }.map do |(time_period, events_for_time_period)|
      [time_period, events_for_time_period.partition { |event| internal_users.include?(event.originating_user.display_name) }]
    end
    File.open("#{event_type.gsub(' ', '_')}_by_#{date_type}.tsv", 'w') do |f|
      f.puts [
        date_type,
        'year',
        'internal_count',
        'external_count',
        'percent_external_for_time_period',
        'total_internal',
        'total_external',
        'percent_external_overall'
      ].join("\t")

      total_internal = 0
      total_external = 0
      by_date_and_user.each do |(time_period, (internal, external))|
        total_internal += internal.size
        total_external += external.size
        f.puts [
          time_period.first,
          time_period.last,
          internal.size,
          external.size,
          sprintf("%0.02f", external.size.to_f/(external.size + internal.size) * 100.0),
          total_internal,
          total_external,
          sprintf("%0.02f", total_external.to_f/(total_external + total_internal) * 100.0)
        ].join("\t")
      end
    end
  end
end
