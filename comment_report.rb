def url_for_changeset(sc)
  id_type = sc.moderated.class.to_s.downcase
  id = sc.moderated.id
  detail_url = FrontendRouter.new(id_type, id).url
  detail_url.gsub(/summary$/, "talk/revisions/list/#{sc.id}/summary")
  #https://civic.genome.wustl.edu/#/events/genes/2478/summary/variants/767/talk/revisions/list/3140/summary
end


def url_for_entity(entity)
  id_type = entity.class.to_s.downcase
  id = entity.id
  detail_url = FrontendRouter.new(id_type, id).url
  detail_url.gsub(/summary$/, "talk/comments")
end

comments = Comment.unscoped.group(:commentable_type, :commentable_id)
  .select('comments.commentable_id, comments.commentable_type, count(comments.id) as count')
  .order('count desc').reject { |x| x.count <= 5 }

File.open('entities_with_discussion.tsv', 'w') do |f|
  f.puts ['url', 'name'].join("\t")
  comments.each do |comment|
    if comment.commentable.present?
      if comment.commentable_type == 'SuggestedChange' && comment.commentable.moderated.present?
        f.puts [url_for_changeset(comment.commentable), comment.commentable.name].join("\t")
      elsif comment.commentable_type != 'SuggestedChange'
        f.puts [url_for_entity(comment.commentable), comment.commentable.name].join("\t")
      end
    end
  end
end

