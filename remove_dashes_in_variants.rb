Variant.where(reference_bases: '-').all.each do |variant|
  variant.reference_bases = nil
  variant.save
end

Variant.where(reference_bases: '').all.each do |variant|
  variant.reference_bases = nil
  variant.save
end

Variant.where(variant_bases: '-').all.each do |variant|
  variant.reference_bases = nil
  variant.save
end

Variant.where(variant_bases: '').all.each do |variant|
  variant.reference_bases = nil
  variant.save
end

SuggestedChange.where(status: 'new', moderated_type: 'Variant').all.each do |sc|
  if sc.suggested_changes.keys.include?('variant_bases')
    sc.suggested_changes.delete('variant_bases') if sc.suggested_changes['variant_bases'].last == '-'
    sc.save
  end

  if sc.suggested_changes.keys.include?('reference_bases')
    sc.suggested_changes.delete('reference_bases') if sc.suggested_changes['reference_bases'].last == '-'
    sc.save
  end
end