require 'csv'
require 'cgi'

def url_template(row)
  pmid = row['PMID']
  variant_name = row[row.headers[5]]
  doid = row['Phenotype'] =~ /RCC/i ? "4450" : '14175'
  familial = (row['F'] || '').strip
  origin = (familial  == 'Somatic' || familial == 'Sporadic') ? 'Somatic%20Mutation' : 'Germline%20Mutation'
  if pmid && variant_name
    encoded_variant_name = CGI.escape(variant_name)
    sig = CGI.escape('Uncertain Significance')
    "https://civic.genome.wustl.edu/#/add/evidence/basic?geneId=58&variantName=#{encoded_variant_name}&variantOrigin=#{origin}&pubmedId=#{pmid.gsub('*', '').gsub('"','')}&evidenceType=Predisposing&evidenceDirection=Supports&diseaseName=#{doid}&clinicalSignificance=#{sig}&evidenceLevel=C"
  else
    nil
  end
end


File.open('vhl_with_civic_urls.tsv', 'w') do |f|
  f.puts [
    'cDNA_Position',
    'Unique Case ID',
    'Multiple Mutants in Case',
    'In CIViC?',
    'Set',
    'Mutation Event c.DNA.',
    'Predicted Consequence Protein Change',
    'Mutation Type',
    'Kindred Case',
    'F',
    'Phenotype',
    'Reference',
    'PMID',
    'Notes',
    'CIViC URL'
  ].join("\t")
  #f.puts [
    #'Mutation Event- cDNA',
    #'Predicted consequence- protein change',
    #'Mutation Type',
    #'Kindred/Case',
    #'Familial/Somatic',
    #'Phenotype',
    #'Reference',
    #'PMID',
    #'Notes on duplicate',
    #'CIViC URL'
  #].join("\t")
  #f.puts [
    #'Mutation.Event..c.DNA.',
    #'Predicted.Consequence..p.Protein.Change.',
    #'Mutation.Type',
    #'Kindred.Case',
    #'F',
    #'Phenotype',
    #'Reference',
    #'PMID',
    #'CIViC URL'
  #].join("\t")

  CSV.new(File.open(ARGV[0], 'r'), col_sep: "\t", headers: true, quote_char: "\x00").each do |row|
    if url = url_template(row)
      new_row = *row.fields
      new_row[0].gsub!(' ', '_')
      new_row[0].gsub!(', ', '_')
      f.puts [
        *new_row,
        url
      ].join("\t")
    else
      next
    end
  end
end
