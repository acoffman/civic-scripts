class CustomEid < EvidenceItemTsvPresenter
  def self.objects
    EvidenceItem.eager_load(:disease, :source, :drugs, variant: [:gene])
      .where(id: [31, 424, 560, 567, 568, 569, 574, 577, 603, 742, 774, 780])
  end
end

class GenerateCustomTsvs < GenerateTsvs

  def tsvs_to_generate
    [CustomEid]
  end

  def release_path
    'custom-id-query'
  end

  def filename_prefix
    release_path
  end
end
